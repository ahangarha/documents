# درباره ما

**ما کی هستیم؟**

ما تعدادی فعال علاقه‌مند به نرم‌افزارآزادیم که در حال ایجاد جامعه‌ای کاربری آزاد برای کاربران فارسی زبان هستیم.
افراد تشکیل‌دهنده‌ی گروهمان شامل هر نوع آدمی میشه. از آدم متخصص در زمینه برنامه‌نویسی گرفته تا افراد مسلط به مباحث رایانه‌ای و حتی تازه‌کارا.

**اهداف**

اهداف ما شامل موارد زیر میشه:
* ایجاد جامعه‌ای پویا برای جامعه‌ای کاربری نرم‌افزار آزاد
* گسترش منابع فارسی زبان برای تازه‌کاران
    * تولید محتوای متنی
    * تولید ویدئوکست‌های آموزشی
    * ترجمه‌ی منابع آموزشی
* تولید ویدئوکست 
* تولید پادکست
* ایجاد انجمن برای حل مشکلات و صحبت کاربران
* برگذاری رویداد‌های مرتبط با نرم‌افزارآزاد

**نیازمندی‌های ما**

از اونجا که این پروژه اهدافش گسترش نرم‌افزارآزاد هست، ما نیاز به هر شکل همکاری داریم. از همفکری و نظر دادن گرفته تا برنامه‌نویسی و ترجمه و حتی تولید محتوا.
اگر فکر می‌کنید میتونید موثر واقع بشید این راه‌های ارتباطی با ماست:
* رایانامه: `sosha1996@riseup.net`
* توییتر: `https://twitter.com/Soshaw96`
* تلگرام: `https://t.me/Soshaw`

**اصول ما**

ما دو اصل داریم:
۱. شفافیت
۲. آزادی
تمامی فعالیت‌های ما و پروژه‌های ما از این دو اصل تبعیت می‌کند.

**پیمان نحوه‌ی برخورد ما با مشارکت کننده**

میتوانید از [اینجا](code-of-conduct.md)، پیمان نحوه‌ی برخورد ما با مشارکت کننده را ببینید.
